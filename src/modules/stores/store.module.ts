import { Module } from '@nestjs/common';
import { StoresController } from './store.controller';
import { StoresService } from './store.service';

import { Store, StoreSchema } from '../../schemas/store.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Store.name, schema: StoreSchema }]),
  ],
  controllers: [StoresController],
  providers: [StoresService],
})
export class StoresModule {}
