export type CreateStoreByUserDto = {
  sid: string;
  readonly user_id: string;
  name: string;
  logo: string;
  description: string;
  title: string;
  type_product: string[];
};

export type CreateStoreRequest = Omit<CreateStoreByUserDto, 'sid' | 'user_id'>;
