import { Injectable } from '@nestjs/common';

import { Store } from '../../schemas/store.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FindStoreByUserDto } from './dto/get_stores_by_user.dto';
import { CreateStoreByUserDto } from './dto/create_store_by_user.dto';

@Injectable()
export class StoresService {
  constructor(@InjectModel(Store.name) private storeModel: Model<Store>) {}

  async findAllStoreByUser(findStoreByUserDto: FindStoreByUserDto) {
    const findStore = await this.storeModel.find({
      user_id: findStoreByUserDto.user_id,
    });
    return findStore;
  }

  async createStoreByUser(createStoreByUserDto: CreateStoreByUserDto) {
    const createdStore = new this.storeModel(createStoreByUserDto);
    return createdStore.save();
  }
}
