import { Controller, Get, Post, Req, Res, Body } from '@nestjs/common';
import { Request, Response } from 'express';
import { StoresService } from './store.service';
import { FindStoreByUserDto } from './dto/get_stores_by_user.dto';
import { UserRequest } from '../users/interfaces/user.interface';
import { User } from 'src/schemas/user.schema';
import { RequestBody, UserToken } from 'src/services/interface';
import {
  CreateStoreByUserDto,
  CreateStoreRequest,
} from './dto/create_store_by_user.dto';
import helper from '../users/config/helper-user';
import { generateDigitCode } from 'src/services/helper';

@Controller('stores')
export class StoresController {
  constructor(private readonly storesService: StoresService) {}

  @Post('/getStoresByUser')
  async getStoresByUser(@Req() req: UserRequest, @Res() res: Response) {
    try {
      let { user } = req;
      let findAllStoreByUser: FindStoreByUserDto = {
        user_id: user.sid,
      };
      let stores = await this.storesService.findAllStoreByUser(
        findAllStoreByUser,
      );
      return res.json({ errorCode: 0, data: stores });
    } catch (error) {
      return res.json({ errorCode: 1, errorMsg: error });
    }
  }

  @Post('/createStoreByUser')
  async createStoreByUser(
    @Req() req: RequestBody<CreateStoreRequest>,
    @Res() res: Response,
  ) {
    try {
      let createStoreByUserDto: CreateStoreByUserDto = {
        sid: generateDigitCode(16),
        user_id: req.user.sid,
        ...req.body,
      };
      await this.storesService.createStoreByUser(createStoreByUserDto);
      return res.json({ errorCode: 0 });
    } catch (error) {
      return res.json({ errorCode: 1, errorMsg: error });
    }
  }
}
