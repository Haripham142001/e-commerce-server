import { Controller, Get, Post, Body, Res } from '@nestjs/common';
import { Request, Response } from 'express';

import { AuthService } from 'src/auth.service';

import { UsersService } from './user.service';
import { UserLoginDto } from './dto/user-login.dto';
import { CreateUserDto } from './dto/create-user.dto';

import helperUser from './config/helper-user';
import { generateDigitCode } from 'src/services/helper';
import { UserToken } from 'src/services/interface';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Post('/login')
  async userLogin(@Body() UserLoginDto: UserLoginDto, @Res() res: Response) {
    try {
      let { username, password } = UserLoginDto;
      let findUser = await this.userService.getUserByUsername(UserLoginDto);
      if (!findUser) {
        return res.json({
          errorCode: 1,
          errorMsg: 'Invalid username or password',
        });
      }
      let checkPW = await helperUser.comparePassword(
        password,
        findUser.password,
      );
      if (!checkPW) {
        return res.json({
          errorCode: 1,
          errorMsg: 'Invalid username or password',
        });
      }
      delete findUser.password;
      let dataUserToken: UserToken = findUser;
      let token = await helperUser.generateToken(dataUserToken);
      return res.json({
        errorCode: 0,
        data: {
          token,
          user: findUser,
        },
      });
    } catch (error) {
      return res.json({ errorCode: 1, error });
    }
  }

  @Post('/register')
  async userRegister(
    @Body() CreateUserDto: CreateUserDto,
    @Res() res: Response,
  ) {
    try {
      if (!CreateUserDto.username || !CreateUserDto.password) {
        return res.json({
          errorCode: 400,
          errorMsg: 'Thiếu username hoặc password',
        });
      }
      let findUser = await this.userService.getUserByUsername(CreateUserDto);
      if (findUser) {
        return res.json({
          errorCode: 400,
          errorMsg: 'Đã tồn tại tài khoản trong hệ thống',
        });
      }
      await this.userService.createUser({
        ...CreateUserDto,
        password: await helperUser.hashPassword(CreateUserDto.password),
        sid: generateDigitCode(16),
      });
      return res.json({ errorCode: 0, errorMsg: 'Tạo tài khoản thành công' });
    } catch (error) {
      return res.json({ errorCode: 1, error });
    }
  }
}
