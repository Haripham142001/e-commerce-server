import { UserLoginDto } from './user-login.dto';

export class CreateUserDto extends UserLoginDto {
  readonly sid: string;
}
