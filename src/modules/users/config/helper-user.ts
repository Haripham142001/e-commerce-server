import * as bcrypt from 'bcrypt';
import * as jwt from 'jsonwebtoken';

import { saltRounds } from './constant-user';

const helper: {
  [field: string]: any;
} = {};

helper.hashPassword = async (pw: string) => {
  return await bcrypt.hash(pw, saltRounds);
};

helper.comparePassword = async (pw: string, pwDB: string) => {
  return await bcrypt.compare(pw, pwDB);
};

helper.generateToken = async (data: any) => {
  return jwt.sign(data, 'your_secret_key', { expiresIn: '10h' });
};

export default helper;
