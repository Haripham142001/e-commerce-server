import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { User } from '../../schemas/user.schema';
import { UserLoginDto } from './dto/user-login.dto';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}
  getHello(): string {
    return 'Hello Users!';
  }
  async getUserByUsername(user: UserLoginDto): Promise<User> {
    const findUser = await this.userModel.findOne({
      username: user.username,
    });
    return findUser.toObject();
  }
  async createUser(user: CreateUserDto): Promise<any> {
    const createdUser = new this.userModel(user);
    return createdUser.save();
  }
}
