import { Request } from 'express';
import { UserToken } from 'src/services/interface';

export interface UserRequest extends Request {
  user?: UserToken;
}
