import { Controller, Get, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}
  @Get()
  getProducts() {
    return 'Get all products';
  }

  @Post('/aaaa')
  getProductss(@Req() request: Request, @Res() response: Response): any {
    let arr = this.productsService.getAllProduct();
    console.log(arr);

    return response.json(arr);
  }
}
