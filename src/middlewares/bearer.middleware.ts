import { NestMiddleware } from '@nestjs/common';
import { JwtService } from 'src/services/jwt.service';
import { Reflector } from '@nestjs/core';
import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';

export interface JwtRequest extends Request {
  user?: any;
}

export class BearerMiddleWare implements NestMiddleware {
  constructor(
    private readonly jwtService: JwtService,
    private readonly reflector: Reflector,
  ) {}
  use(req: JwtRequest, res: Response, next: NextFunction) {
    const header = req.headers?.authorization;
    if (!header) return res.json('Unauthorized');

    const token = header.split('Bearer ')?.[1];
    if (!token) return res.json('Unauthorized');
    let expiry: string;
    try {
      const decodedToken: string | jwt.JwtPayload = jwt.verify(
        token,
        'your_secret_key',
      );
      if (typeof decodedToken === 'string') {
        const jsonDecodedToken: jwt.JwtPayload = JSON.parse(decodedToken);
        expiry = this.validateTokenExpiration(jsonDecodedToken);
        req.user = jsonDecodedToken;
      } else {
        expiry = this.validateTokenExpiration(decodedToken);
        req.user = decodedToken;
      }
    } catch (error) {
      return res.json({ errorCode: 403, errorMsg: 'Unauthorized' });
    }
    if (expiry === 'Unauthorized') {
      return res.json({ errorCode: 403, errorMsg: 'Unauthorized' });
    }
    return next();
  }
  private validateTokenExpiration(decodedToken: jwt.JwtPayload) {
    if (decodedToken.exp && Date.now() >= decodedToken.exp * 1000) {
      return 'Unauthorized';
    }
    return null;
  }
}
