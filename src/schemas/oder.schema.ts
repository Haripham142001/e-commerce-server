import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';
import { Product } from './product.schema';

export type OrderDocument = HydratedDocument<Order>;

@Schema()
export class Order {
  @Prop({ required: true, unique: true })
  sid: string;

  @Prop()
  user_id: string;

  @Prop()
  products: {
    name: string;
    price: string;
    discount: string;
    timeAdd: Date;
    quantity: number;
    size?: string;
    color?: string;
    store_id?: string;
  }[];
}

export const OrderSchema = SchemaFactory.createForClass(Order);
