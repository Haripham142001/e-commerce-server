import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type Category_StoreDocument = HydratedDocument<Category_Store>;

@Schema()
export class Category_Store {
  @Prop({ required: true, unique: true })
  sid: string;

  @Prop({ required: true })
  name: string;

  @Prop()
  icon: string;

  @Prop({ default: 'active' })
  status: string;

  @Prop({ required: true })
  store_id: string;
}

export const Category_StoreSchema =
  SchemaFactory.createForClass(Category_Store);
