import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Date, HydratedDocument } from 'mongoose';

export type ProductDocument = HydratedDocument<Product>;

@Schema()
export class Product {
  @Prop({ required: true, unique: true })
  sid: string;

  @Prop({ required: true })
  name: string;

  @Prop()
  image: string[];

  @Prop()
  price: string[];

  @Prop({ default: false })
  isSale: boolean;

  @Prop()
  dateSale: string;

  @Prop()
  discount: string[];

  @Prop()
  description: string;

  @Prop({ required: true })
  store_id: string;

  @Prop({ default: 'active' })
  status: string;

  @Prop({ required: true })
  category_id: string;

  @Prop()
  options: Array<{
    color?: string;
    images: string[];
    price?: string;
    discout?: string;
    number: number;
    quality?: string;
    quantity?: number;
    unit: string;
  }>;

  @Prop()
  origin: string;

  @Prop()
  evaluate: Array<{
    user_id: string;
    images: string[];
    content: string;
    rate: number;
    createdAt: Date;
  }>;
}

export const ProductSchema = SchemaFactory.createForClass(Product);
