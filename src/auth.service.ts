import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class AuthService {
  // Replace this method with your actual user validation logic (e.g., checking against the database)
  async validateUserById(userId: number): Promise<any> {
    // Your validation logic goes here
    // Return the user object if it exists, or return null if the user doesn't exist
    return null;
  }

  // Replace this method with your actual user validation logic (e.g., checking against the database)
  async validateUserByUsername(
    username: string,
    password: string,
  ): Promise<any> {
    // Your validation logic goes here
    // Return the user object if the username and password are valid, or return null if they are not valid
    return null;
  }

  // Create a method to generate the JWT token for the authenticated user
  async generateToken(user: any): Promise<string> {
    const payload = { sub: user.id, username: user.username }; // Customize the payload as needed
    return jwt.sign(payload, 'your_secret_key', { expiresIn: '1h' }); // Same secret key and expiresIn as in AppModule
  }
}
