import { Request } from 'express';
import { User } from 'src/schemas/user.schema';

type UserToken = Omit<User, 'password'>;

interface RequestBody<T> extends Request {
  body: T;
  user?: UserToken;
}

export { UserToken, RequestBody };
